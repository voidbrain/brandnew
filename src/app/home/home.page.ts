import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
    constructor(
        public http: HttpClient
    ) {}
 
    ngOnInit() {
        let params = {};
        this.http.get('https://www.hultimate.net/ajax/moduli/api/user', { params: params }).toPromise().then((response) => {
              console.log(response);
        });
      }
}
